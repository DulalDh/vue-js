new Vue({
    el: '#app',
    data: {
        playerHealth: 100,
        monsterHealth: 100,
        gameIsRunning: false,
        turns: []
    },

    methods: {
        attack: function()
        {
            var damage = this.calculateDamage(10, 3);
            this.monsterHealth -= damage;
            this.monsterAttack();
            this.turns.unshift({
                isPlayer: true,
                text: 'Player hits Monster for ' + damage
            });
            if(this.winCheck() )
            {
                this.gameIsRunning = false;
                this.clear();
                return;
            }
        },
        
        specialAttack: function()
        {
            this.monsterHealth -= this.calculateDamage(20, 10);
            this.monsterAttack();
            this.turns.unshift({
                isPlayer: true,
                text: 'Player hits Monster hard for ' + damage
            });
            if(this.winCheck() )
            {
                this.gameIsRunning = false;
                this.clear();
                return;
            }
        },
        heal: function()
        {
            if(this.playerHealth<=90){
               this.playerHealth+=10;
            }else{
                this.playerHealth = 100;
            }
            this.turns.unshift({
                isPlayer: true,
                text: 'Player heals for 10'
            });
            this.monsterAttack();

        },

        gameStart: function()
        {
            this.gameIsRunning = true;
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.turns = [];
        },
        clear: function()
        {
            this.gameIsRunning = false;
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.turns = [];
        },
        
        giveUp: function()
        {
            this.gameIsRunning = false;
        },
        
        monsterAttack: function()
        {
            var damage =  this.calculateDamage(12, 5);
            this.playerHealth -= damage;
            this.turns.unshift({
                isPlayer: false,
                text: 'Monstwr hits player for ' + damage
            });

        },

        calculateDamage: function(max, min)
        {
            return  Math.max(Math.floor(Math.random()*max), min) 
        },

        winCheck: function()
        {
            if(this.playerHealth<=0)
            {
                if (confirm('You won! New Game?')) {
                    this.gameStart();
                }
                else{
                return true; }
            }
            if(this.monsterHealth<=0)
            {
                if (confirm('You loss! New Game?')) {
                    this.gameStart();
                }
                else{
                return true; }
            }

            return false;

        }

    }
});
